package chapter1;

import chapter1.BraveKnight;
import chapter1.Quest;
import org.junit.Test;
import static org.mockito.Mockito.*;


/**
 * @author YangShuai
 * @create 2017/9/6
 * @update 2017/9/6
 */
public class BraveKnightTest {

    @Test
    public void knightShouldEmbarkOnQuest(){
        Quest quest = mock(Quest.class);
        BraveKnight knight = new BraveKnight(quest);
        knight.embarkOnQuest();
        verify(quest,times(1)).embark();
    }
}

package chapter1;

/**
 * @author YangShuai
 * @create 2017/9/6
 * @update 2017/9/6
 */
public interface Knight {
    void embarkOnQuest();
}

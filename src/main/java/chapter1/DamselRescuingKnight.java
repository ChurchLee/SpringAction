package chapter1;

/**
 * @author YangShuai
 * @create 2017/9/6
 * @update 2017/9/6
 */
public class DamselRescuingKnight implements Knight {

    private RescueDamseQuest quest;

    public DamselRescuingKnight() {
        this.quest = new RescueDamseQuest();
    }

    public void embarkOnQuest() {
        quest.embark();
    }
}

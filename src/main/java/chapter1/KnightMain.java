package chapter1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author YangShuai
 * @create 2017/9/6
 * @update 2017/9/6
 */
public class KnightMain {

    public static void main(String[] args){
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("minstrel.xml");
        Knight knight = context.getBean(Knight.class);
        knight.embarkOnQuest();
        context.close();
    }
}

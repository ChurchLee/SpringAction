package chapter1;

/**
 * @author YangShuai
 * @create 2017/9/6
 * @update 2017/9/6
 */
public class BraveKnight implements Knight {
    private Quest quest;

    public BraveKnight(Quest quest) {
        this.quest = quest;
    }

    public void embarkOnQuest() {
        quest.embark();
    }
}

package chapter1;

import chapter1.Quest;

import java.io.PrintStream;

/**
 * @author YangShuai
 * @create 2017/9/6
 * @update 2017/9/6
 */
public class SlayDragonQuest implements Quest {

    private PrintStream stream;

    public SlayDragonQuest(PrintStream stream) {
        this.stream = stream;
    }

    public void embark() {
        stream.println("Embarking on quest to slay the dragon!");
    }
}

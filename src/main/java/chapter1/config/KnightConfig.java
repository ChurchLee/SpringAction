package chapter1.config;

import chapter1.BraveKnight;
import chapter1.Knight;
import chapter1.Quest;
import chapter1.SlayDragonQuest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author YangShuai
 * @create 2017/9/6
 * @update 2017/9/6
 */
@Configuration
public class KnightConfig {

    @Bean
    public Knight knight(){
        return new BraveKnight(quest());
    }

    @Bean
    public Quest quest(){
        return new SlayDragonQuest(System.out);
    }
}

package practice;

/**
 * @author YangShuai
 * @create 2017/9/7
 * @update 2017/9/7
 */
public class Base {

    private Description description = new Description("test");

    public Base() {
        System.out.println("Base constructor");
    }

    public static void main(String[] args){
        Derived derived = new Derived();
        //base.f();
    }
    private void f(){
        System.out.println("Base private method f()");
    }
}

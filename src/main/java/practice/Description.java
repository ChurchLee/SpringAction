package practice;

/**
 * @author YangShuai
 * @create 2017/9/7
 * @update 2017/9/7
 */
public class Description {

    private String string;

    public Description(String string) {
        this.string = string;
        System.out.println("Description :"+string);
    }
}

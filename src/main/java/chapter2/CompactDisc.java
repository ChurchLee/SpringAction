package chapter2;

/**
 * @author YangShuai
 * @create 2017/9/6
 * @update 2017/9/6
 */
public interface CompactDisc {

    void play();
}

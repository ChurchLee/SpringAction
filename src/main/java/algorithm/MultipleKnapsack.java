package algorithm;

/**
 * @author YangShuai
 * @create 2017/9/8
 * @update 2017/9/8
 */

//多重背包问题
public class MultipleKnapsack {

    public static void main(String[] args) {
        int[] weight = {2, 3, 4, 7};//物品体积
        int[] value = {2, 5, 3, 9};//物品价值
        int container = 10;//背包容量
        int f[][] = new int[5][11];
        int[] number = {8, 1, 1, 1};//物品数量
        for (int i = 1; i <= weight.length; i++) {
            for (int n = 1;n <= Math.min(number[i-1],container/weight[i-1]);n++){
                for (int v = 0;v <= container;v++){
                    if (v >= n*weight[i-1]){
                        f[i][v] = Math.max(f[i-1][v - n*weight[i-1]]+n*value[i-1],f[i-1][v]);
                    }else {
                        f[i][v] = Math.max(f[i-1][v],f[i][v]);
                    }
                }
            }
        }
        new MultipleKnapsack().oneDegree();
        System.out.println();
    }

    public void oneDegree(){
        int[] weight = {2, 3, 4, 7};//物品体积
        int[] value = {2, 5, 5, 9};//物品价值
        int container = 10;//背包容量
        int f[] = new int[11];
        int[] number = {1, 1, 1, 1};//物品数量
        for (int i = 1;i<=weight.length;i++){
            int n = 1;
            for (int v = weight[i-1];v <=container;v++){
                if (n <= Math.min(number[i-1],container/weight[i-1])){
                    f[v] = Math.max(f[v-n*weight[i-1]]+n*value[i-1],f[v]);
                    n++;
                }else {
                    f[v] = f[n*weight[i-1]];
                }
            }
        }

        System.out.println();
    }
}

package algorithm;

/**
 * @author YangShuai
 * @create 2017/9/7
 * @update 2017/9/7
 */
public class CoinProblem {

    public static void main(String[] args){
        int[] coinValues = {1,3,5};
        int values = 5;//需要找零的币值
        int coinUsed = 0;
        for(int i = coinValues.length - 1;i >= 0 ;i--){
            if (values >= coinValues[i]){
                values -= coinValues[i];
                coinUsed++;
                i = coinValues.length;
            }
            if (values == 0)
                break;
        }
        System.out.println(coinUsed);
    }
}

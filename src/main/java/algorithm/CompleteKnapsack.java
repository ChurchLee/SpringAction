package algorithm;

/**
 * @author YangShuai
 * @create 2017/9/8
 * @update 2017/9/8
 */

//完全背包问题
public class CompleteKnapsack {

    //当v从当前物品的体积开始递增，这样可以放入多个当前物品，
    // 因为低容量的时候先放入了物品，到高容量时是低容量的价值加上当前物品的价值
    public static void main(String[] args){
        int[] weight = {1,2,4,7};
        int[] value ={1,3,7,9};
        int container = 10;
        int f[] = new int[11];
        for (int i = 1;i <= weight.length;i++){
            for (int v = weight[i-1]; v <=container;v++){
                f[v] = Math.max(f[v-weight[i-1]]+value[i-1],f[v]);
            }
        }
        System.out.println();
    }
}

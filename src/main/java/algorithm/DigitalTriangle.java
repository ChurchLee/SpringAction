package algorithm;

/**
 * @author YangShuai
 * @create 2017/9/7
 * @update 2017/9/7
 */
public class DigitalTriangle {

    public static void main(String[] args){
        int[][] array = {{7},{3,8},{8,1,0},{2,7,4,4},{4,5,2,6,5}};
        int[] maxArray = array[4];
        for (int i = array.length-2;i >= 0 ;i--){
            for (int k = 0; k < array[i].length;k++){
                maxArray[k] = Math.max(maxArray[k],maxArray[k+1])+array[i][k];
            }
        }
        for (int i = 0;i < maxArray.length;i++){
            System.out.println(maxArray[i]);
        }


    }
}

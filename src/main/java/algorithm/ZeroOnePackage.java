package algorithm;

/**
 * @author YangShuai
 * @create 2017/9/7
 * @update 2017/9/7
 */
public class ZeroOnePackage {



    //二维数组解法
    //处理：i表示处理第几件物品，v表示背包的容量
    //如果背包的容量小于当前物品的体积，不会被放入
    //是否放入当前这件物品是判断前i-1件物品处理完后剩下的容量为v-weight[i]的背包价值
    // 加上当前物品的价值是否大于不放入这件物品的背包时的价值
    public static void main(String[] args){
        int[] weight = {5,6,4};
        int[] values = {20,10,12};
        int container = 10;
        int[][] f = new int[4][11];
        for(int i = 1;i <=3;i++ ){
            for (int v = container;v >=weight[i-1];v--){
             f[i][v] = Math.max(f[i-1][v-weight[i-1]]+values[i-1],f[i-1][v]);
            }
        }
        new ZeroOnePackage().zeroOnePackageOneDegree(weight,values,container);
    }


    //一维数组解法
    //v表示背包当前容量，f[v]表示当前容量中物品的价值
    public void zeroOnePackageOneDegree(int[] weight,int[] values,int container){
        int[] f = new int[11];
        for (int i = 1;i<=weight.length;i++){
            for (int v = container;v >=weight[i-1];v--){
                f[v] = Math.max(f[v-weight[i-1]]+values[i-1],f[v]);
            }
        }
    }
}
